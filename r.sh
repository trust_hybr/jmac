#!/bin/bash

docker ps | grep jmac_c
echo ---------------------------------------------------------------------------------
docker container stop jmac_c
docker container rm jmac_c
sleep 3
echo ---------------------------------------------------------------------------------
docker run -d --restart=always -p 8100:80 --name jmac_c jmac:latest 
sleep 3
echo ---------------------------------------------------------------------------------
docker ps | grep jmac
