forms = {

    "user_login":
    {
        "title": "Login",
        "detail": "You must authenticate to use the BandS application",
        "save_button_text": "Login",
        "first_step_field": "phone_number",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Networks",
        "steps": [
        {
            "number": "1",
            "title": "Phone Number",
            "detail": "Provide your phone number along with country code",
            "next_step_field": "send_otp_button",
            "required": true,
            "minimumLength": 11,
            "show_in_final_step": true,
            "default_value": "91",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Send OTP",
            "detail": "By clicking this button you will receive OTP in SMS inbox",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 0,
            "show_in_final_step": false,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false

        },
        {
            "number": "3",
            "title": "OTP",
            "detail": "Please type the received OTP",
            "next_step_field": "login",
            "required": true,
            "minimumLength": 4,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false
        }]
    },

    "visit_plan":
    {
        "title": "Visit",
        "detail": "Please provide the information to schedule your visit. This will help you to reduce your time at the entrance gate.",
        "save_button_text": "Save Visit Plan",
        "first_step_field": "org_location",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Manage Visitor",
        "steps": [
        {
            "number": "9",
            "title": "Location",
            "detail": "Please select the location you are visiting",
            "next_step_field": "radio",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "10",
            "title": "Visitor is",
            "detail": "Select relation of visitor with host or organization",
            "data": [
                {"key": "Guest", "value": "Guest"},
                {"key": "Customer", "value": "Customer"},
                {"key": "Vendor", "value": "Vendor"},
                {"key": "Supplier", "value": "Supplier"},
                {"key": "Marketing", "value": "Marketing"},
                {"key": "Other", "value": "Other"},
            ],
            "next_step_field": "phone_number",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "Female",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "1",
            "title": "Visitor Phone Number",
            "detail": "Please provide visitor phone number",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 11,
            "show_in_final_step": true,
            "default_value": "91",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "7",
            "title": "Visitor Name",
            "detail": "Name of Visiting Person",
            "next_step_field": "camera",
            "required": true,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },        
        {
            "number": "8",
            "title": "Visitor Photo",
            "detail": "Photo of Visiting Person",
            "next_step_field": "autocomplete",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Contact of Host",
            "detail": "Please select the contact you are visiting",
            "next_step_field": "date",
            "tableName": "contact",
            "searchFields": ['text-1', 'phone_number-2'],
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "Date of Visit",
            "detail": "For which date the visit is planned",
            "next_step_field": "time",
            "required": false,
            "minimumLength": 10,
            "show_in_final_step": true,
            "default_value": "_now_date_",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "number": "4",
            "title": "Time of Visit",
            "detail": "At what time the visit will start",
            "next_step_field": "integer",
            "required": false,
            "minimumLength": 5,
            "show_in_final_step": true,
            "default_value": "_now_time_",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "number": "5",
            "title": "Number of Days",
            "detail": "For how many days visit is planned",
            "next_step_field": "textarea",
            "required": false,
            "minimumLength": 1,
            "min" : 1,
            "max" : 365,
            "step" : 7,
            "show_in_final_step": true,
            "default_value": "1",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "number": "6",
            "title": "Purpose of Visit",
            "detail": "Why this visit is planned?",
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "Meeting with host",
            "show_in_list": false,
            "show_in_option": false
        }]
    },

    "visit_approval":
    {
        "title": "Approval",
        "detail": "Search the plan by visitor or host phone number and then make sure it is approved. If it is not approved talk to host and get it approved.",
        "save_button_text": "Save Visit Approval",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Manage Visitor",
        "steps": [
        {
            "foreign_table": "visit_plan",
            "number": "1",
            "title": "Visit",
            "detail": "Select the visit for approval",
            "next_step_field": "boolean",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Is approved?",
            "detail": "Select Yes for approval and No for rejection",
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "Yes",
            "show_in_list": true,
            "show_in_option": true
        }]
    },

    "visit_leaving":
    {
        "title": "Leaving",
        "detail": "Search the plan by visitor or host phone number and then make sure it is marked for exit.",
        "save_button_text": "Save Visit Exit",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Manage Visitor",
        "steps": [
        {
            "foreign_table": "visit_plan",
            "number": "1",
            "title": "Visit",
            "detail": "Select the visit for which person is exiting",
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }
        ]
    },

    "organization":
    {
        "title": "Organization",
        "detail": "Information about your organization",
        "save_button_text": "Save Organization",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization",
        "steps": [
        {
            "number": "1",
            "title": "Full Legal Name",
            "detail": "Full name of organization",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Abbreviation",
            "detail": "Abbreviation of organization",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "Statement",
            "detail": "Statement of organization",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 5,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false
        }]
    },

    "organization_branch":
    {
        "title": "Branch",
        "detail": "Information about your organization branch",
        "save_button_text": "Save Organization Branch",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization Location",
        "steps": [
        {
            "foreign_table": "organization",
            "number": "1",
            "title": "Organization",
            "detail": "Select the organiztion for this branch",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Name",
            "detail": "Name of organization branch",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "Code",
            "detail": "Code of branch",
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }]
    },

    "organization_branch_building":
    {
        "title": "Building",
        "detail": "Information about your organization building",
        "save_button_text": "Save Organization Building",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization Location",
        "steps": [
        {
            "foreign_table": "organization_branch",
            "number": "1",
            "title": "Organization Branch",
            "detail": "Select the organiztion branch of this building",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Name",
            "detail": "Name of organization building",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }]
    },


    "organization_building_floor":
    {
        "title": "Floor",
        "detail": "Information about your organization building floor",
        "save_button_text": "Save Organization Building Floor",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization Location",
        "steps": [
        {
            "foreign_table": "organization_branch_building",
            "number": "1",
            "title": "Organization Building",
            "detail": "Select the organiztion building for this floor",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Number",
            "detail": "Number of organization building floor",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }]
    },

    "organization_floor_section":
    {
        "title": "Section or Flat",
        "detail": "Information about your organization building section or home",
        "save_button_text": "Save Section or Home",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization Location",
        "steps": [
        {
            "foreign_table": "organization_building_floor",
            "number": "1",
            "title": "Organization Building Floor",
            "detail": "Select the organiztion building floor of this section or home",
            "next_step_field": "text",
            "required": false,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Number",
            "detail": "Number of organization building section or flat",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },      
        {
            "number": "3",
            "title": "Postal Address",
            "detail": "Postal address of section or flat",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 10,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }]
    },


    "organization_team":
    {
        "title": "Team",
        "detail": "Information about teams and groups in the organization",
        "save_button_text": "Save Team",
        "first_step_field": "text",
        "parent_field": "organization_team-4",
        "action_after_final_step": "/actions.html?a=Organization Member",
        "steps": [
        {
            "number": "2",
            "title": "Full Name",
            "detail": "Full name of team",
            "next_step_field": "textarea",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "1",
            "title": "About Team",
            "detail": "Detail of what team does",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 10,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_team",
            "number": "4",
            "title": "Parent Team",
            "detail": "Select the parent team",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false
        }]
    },

    "organization_worker_level":
    {
        "title": "Levels",
        "detail": "Information about various worker levels in the organization",
        "save_button_text": "Save Member Level",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization Member",
        "steps": [
        {
            "number": "1",
            "title": "Name",
            "detail": "Name of organization level",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }]
    },

   "organization_worker":
    {
        "title": "Member",
        "detail": "Information about organization member or worker",
        "save_button_text": "Save Member",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Organization Member",
        "steps": [
        {
            "foreign_table": "contact",
            "number": "8",
            "title": "Contact Person",
            "detail": "Select the person from contacts",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "foreign_table": "organization",
            "number": "1",
            "title": "Organization",
            "detail": "Select the organiztion of this person",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_team",
            "number": "2",
            "title": "Team",
            "detail": "Select the organiztion team of this role",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_worker_level",
            "number": "3",
            "title": "Level",
            "detail": "Select the organiztion worker level of this role",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_branch",
            "number": "4",
            "title": "Branch",
            "detail": "Select the organiztion branch of this person",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "Any",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_branch_building",
            "number": "5",
            "title": "Building",
            "detail": "Select the organiztion building of this person",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "Any",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_building_floor",
            "number": "6",
            "title": "Floor",
            "detail": "Select the organiztion floor of this person",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "Any",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "foreign_table": "organization_floor_section",
            "number": "7",
            "title": "Area",
            "detail": "Select the organiztion area of this person",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "Any",
            "show_in_list": true,
            "show_in_option": false
        }
        ]
    },
     

    "contact":
    {
        "title": "Contact",
        "detail": "Information about your contacts. Where phone number of contact and current login phone number matches then that will be your personal profile.",
        "save_button_text": "Save Contact Information",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=My Profile",
        "steps": [
        {
            "number": "1",
            "title": "Name",
            "detail": "Your full name",
            "next_step_field": "phone_number",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Phone Number",
            "detail": "Provide your phone number along with country code",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 11,
            "show_in_final_step": true,
            "default_value": "91",
            "show_in_list": true,
            "show_in_option": true
        }]
    },


    "oorja_room":
    {
        "title": "Rooms",
        "detail": "List of rooms where 'room supervisors' and 'central manager' will be installed",
        "save_button_text": "Save Room",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Save Energy",
        "steps": [
        {
            "number": "1",
            "title": "Name",
            "detail": "name of room",
            "next_step_field": "boolean",
            "required": true,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Central Location",
            "detail": "Select Yes or No if this room or area is for 'central manager'",
            "next_step_field": "text",
            "required": false,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "Yes",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "3",
            "title": "Serial Number",
            "detail": "serial number of device which will be installed",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 6,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "foreign_table": "wifi_network",
            "number": "1",
            "title": "Network to Use",
            "detail": "Select the Wi-Fi network to be used",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        ]
    },

    "oorja_switch":
    {
        "title": "Switches",
        "detail": "List of switches in room where 'room supervisors' will be installed",
        "save_button_text": "Save Switch",
        "first_step_field": "radio",
        "parent_field": "",
        "action_after_final_step": "/actions.html?a=Save Energy",
        "steps": [
        {
            "number": "1",
            "title": "Connected Device",
            "detail": "Select the connected device with switch",
            "data": [
                {"key": "Bulb", "value": "Bulb"},
                {"key": "Tubelight", "value": "Tubelight"},
                {"key": "Table Lamp", "value": "Table Lamp"},
                {"key": "Night Light", "value": "Night Light"},
                {"key": "Light Dimmer", "value": "Light Dimmer"},
                {"key": "Cooler", "value": "Cooler"},
                {"key": "Fan", "value": "Fan"},
                {"key": "Cooler", "value": "Cooler"},
                {"key": "Air Conditioner", "value": "Air Conditioner"},
                {"key": "Television", "value": "Television"},
                {"key": "Plug Socket", "value": "Plug Socket"},
                {"key": "Other", "value": "Other"},
            ],
            "next_step_field": "integer",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "Female",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Pin Number",
            "detail": "Define the pin number of 'room supervisor' connected to switch",
            "next_step_field": "foreign_key",
            "required": true,
            "minimumLength": 1,
            "min" : 1,
            "max" : 8,
            "step" : 1,
            "show_in_final_step": true,
            "default_value": "1",
            "show_in_list": false,
            "show_in_option": false
        },
        {
            "foreign_table": "oorja_room",
            "number": "3",
            "title": "Room",
            "detail": "Select the room for this switch",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        }
        ]
    },

    "oorja_remote":
    {
        "title": "Remote",
        "detail": "Oorja remote to manage the switches",
        "save_button_text": "Run",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=oorja_remote",
        "steps": [
        {
            "foreign_table": "oorja_switch",
            "number": "1",
            "title": "Switch",
            "detail": "Select the switch",
            "next_step_field": "radio",
            "required": true,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "What",
            "detail": "Select the connected device with switch",
            "data": [
                {"key": "On", "value": "On"},
                {"key": "Off", "value": "Off"},
                {"key": "Switch Not Working", "value": "Switch Not Working"},
                {"key": "Socket Not Working", "value": "Socket Not Working"},
                {"key": "Device Not Working", "value": "Device Not Working"},
            ],
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "On",
            "show_in_list": true,
            "show_in_option": true
        },
        ]
    },

    "wifi_network":
    {
        "title": "Wi-Fi Networks",
        "detail": "List of Wi-Fi networks for use",
        "save_button_text": "Save WiFi",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=wifi_network",
        "steps": [
        {
            "number": "2",
            "title": "SSID",
            "detail": "A Service Set Identifier (SSID) is a unique ID that consists of 32 characters and is used for naming wireless networks",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "User Name",
            "detail": "Username or User ID used to login the Wi-Fi network",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "4",
            "title": "User Password",
            "detail": "User password used to login the Wi-Fi network",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 2,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        
        ]
    },

    "pets_animal":
    {
        "title": "Animal",
        "detail": "Information about pets",
        "save_button_text": "Save Animal",
        "first_step_field": "foreign_key",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=pets_animal",
        "steps": [
        {
            "foreign_table": "organization",
            "number": "1",
            "title": "Organization",
            "detail": "Select the owner organiztion",
            "next_step_field": "autocomplete",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Type",
            "detail": "Select the animal type",
            "tableName": "pets_animal_type",
            "searchFields": ['text-1'],            
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "On",
            "show_in_list": true,
            "show_in_option": true
        },        
        {
            "number": "5",
            "title": "Name",
            "detail": "Name of the animal",
            "next_step_field": "integer",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "Count",
            "detail": "How many animals to add? Type 1 if only one animal.",
            "next_step_field": "integer",
            "required": true,
            "minimumLength": 1,
            "min" : 1,
            "max" : 500,
            "step" : 1,
            "show_in_final_step": true,
            "default_value": "1",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "4",
            "title": "Serial Number",
            "detail": "Any serial number associated",
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 0,
            "min" : 1,
            "max" : 500,
            "step" : 1,
            "show_in_final_step": true,
            "default_value": "1",
            "show_in_list": true,
            "show_in_option": true
        }]
    },

    "pets_animal_type":
    {
        "title": "Animal Types",
        "detail": "Information about animal types",
        "save_button_text": "Save Animal Type",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=pets_animal_type",
        "steps": [
        {
            "number": "1",
            "title": "Name",
            "detail": "Name of animal type",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        ]
    },

    "fact":
    {
        "title": "Fact Types",
        "detail": "Fact used to describe a property",
        "save_button_text": "Save Fact",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=fact",
        "steps": [
        {
            "number": "1",
            "title": "Fact Name",
            "detail": "Name of fact",
            "next_step_field": "radio",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Value Type",
            "detail": "What type of value it will store",
            "data": [
                {"key": "String", "value": "text"},
                {"key": "Boolean", "value": "boolean"},
                {"key": "Date", "value": "date"},
                {"key": "Time", "value": "time"},
                {"key": "Text", "value": "textarea"},
                {"key": "Integer", "value": "integer"},
                {"key": "Postal Address", "value": "postal_address"},
                {"key": "Email Address", "value": "email_address"},
                {"key": "Phone Number", "value": "phone_number"},
                {"key": "Organization Location", "value": "org_location"},
            ],
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "text",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "Validation",
            "detail": 'Property list of validations. e.g. ' + 
                ' min_integer=1, max_integer=10, integer_step=1' +
                ' min_string_length=2, max_string_length=20' +
                ' min_date_in_days=0, max_date_in_days=7' +
                ' min_time_in_minutes=0, max_time_in_minutes=60',
            "next_step_field": "text",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "max_string_length=20",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "4",
            "title": "Data",
            "detail": 'Comma seperated acceptable values for fact',
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },   
        ]
    },

    "event":
    {
        "title": "Event Types",
        "detail": "Event used to describe a property",
        "save_button_text": "Save Event",
        "first_step_field": "text",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=event",
        "steps": [
        {
            "number": "1",
            "title": "Event Name",
            "detail": "Name of event",
            "next_step_field": "radio",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "2",
            "title": "Value Type",
            "detail": "What type of value it will store",
            "data": [
                {"key": "String", "value": "text"},
                {"key": "Boolean", "value": "boolean"},
                {"key": "Date", "value": "date"},
                {"key": "Time", "value": "time"},
                {"key": "Text", "value": "textarea"},
                {"key": "Integer", "value": "integer"},
                {"key": "Postal Address", "value": "postal_address"},
                {"key": "Email Address", "value": "email_address"},
                {"key": "Phone Number", "value": "phone_number"},                
                {"key": "Organization Location", "value": "org_location"},
            ],
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "date",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "3",
            "title": "Validation",
            "detail": 'Property list of validations. e.g. ' + 
                ' min_integer=1, max_integer=10, integer_step=1' +
                ' min_string_length=2, max_string_length=20' +
                ' min_date_in_days=0, max_date_in_days=7' +
                ' min_time_in_minutes=0, max_time_in_minutes=60',
            "next_step_field": "text",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": " max_date_in_days=0",
            "show_in_list": true,
            "show_in_option": true
        },
        {
            "number": "4",
            "title": "Data",
            "detail": 'Comma seperated acceptable values for event',
            "next_step_field": "final_step",
            "required": false,
            "minimumLength": 0,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": true
        },   
        ]
    },

    "pets_animal_fact":
    {
        "title": "Animal Fact",
        "detail": "Information about pet facts",
        "save_button_text": "Save Animal Fact",
        "first_step_field": "autocomplete",
        "parent_field": "",
        "action_after_final_step": "/list.html?l=pets_animal_fact",
        "steps": [
        {
            "number": "1",
            "title": "Animal",
            "tableName": "pets_animal",
            "searchFields": ['organization-1', 'autocomplete-2', 'text-5', 'integer-4'],            
            "detail": "Select the animal for which this fact is defined",
            "next_step_field": "autocomplete",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },
        {
            "number": "2",
            "title": "Fact",
            "tableName": "fact",
            "searchFields": ['text-1'],       
            "detail": "Select the fact for which this fact is defined",
            "next_step_field": "text",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "",
            "show_in_list": true,
            "show_in_option": false
        },        
        {
            "number": "3",
            "title": "Value",
            "detail": "Value of fact",
            "next_step_field": "final_step",
            "required": true,
            "minimumLength": 1,
            "show_in_final_step": true,
            "default_value": "1",
            "show_in_list": true,
            "show_in_option": true
        }
        ]
    },

    


}; // forms