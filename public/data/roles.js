roles = [
    {
        "tables": [
            "visit_plan"
        ],
        "authorizedRoles": [
            ["092f73924152ba366a3470661e9305f2", "d44ec2fe2b648365487118dbfb04a812"], // Entrance Gate, Staff
            ["092f73924152ba366a3470661e9305f2", "9370096149cfc279479c6fa6ce6e0da3"], // Entrance Gate, Supervisor
            ["092f73924152ba366a3470661e9305f2", "21692d7c7a28bde644ac6f015ca85e12"], // Entrance Gate, Manager
            ["6903790c984e85d292a2a19655d439fc", "d44ec2fe2b648365487118dbfb04a812"], // Exit Gate, Staff
            ["6903790c984e85d292a2a19655d439fc", "9370096149cfc279479c6fa6ce6e0da3"], // Exit Gate, Supervisor
            ["6903790c984e85d292a2a19655d439fc", "21692d7c7a28bde644ac6f015ca85e12"], // Exit Gate, Manager
            ["45090411c65556c9fbf753427c05eb46", "21692d7c7a28bde644ac6f015ca85e12"], // Security, Manager
            ["17c74d4cf54dd9666719f3fa8d7a5d13", "8530aa16eb44b78690297536ca60df0c"], // testing Officers, Secaretry
        ]
    },
    {
    	"tables": [
            "visit_approval"
    	],
    	"authorizedRoles": [
            ["092f73924152ba366a3470661e9305f2", "d44ec2fe2b648365487118dbfb04a812"], // Entrance Gate, Staff
            ["092f73924152ba366a3470661e9305f2", "9370096149cfc279479c6fa6ce6e0da3"], // Entrance Gate, Supervisor
            ["092f73924152ba366a3470661e9305f2", "21692d7c7a28bde644ac6f015ca85e12"], // Entrance Gate, Manager
            ["45090411c65556c9fbf753427c05eb46", "21692d7c7a28bde644ac6f015ca85e12"], // Security, Manager
            ["17c74d4cf54dd9666719f3fa8d7a5d13", "8530aa16eb44b78690297536ca60df0c"], // testing Officers, Secaretry
        ]
    },
    {
    	"tables": [
    		"visit_leaving"
    	],
    	"authorizedRoles": [
            ["6903790c984e85d292a2a19655d439fc", "d44ec2fe2b648365487118dbfb04a812"], // Exit Gate, Staff
            ["6903790c984e85d292a2a19655d439fc", "9370096149cfc279479c6fa6ce6e0da3"], // Exit Gate, Supervisor
            ["6903790c984e85d292a2a19655d439fc", "21692d7c7a28bde644ac6f015ca85e12"], // Exit Gate, Manager
            ["45090411c65556c9fbf753427c05eb46", "21692d7c7a28bde644ac6f015ca85e12"], // Security, Manager
        ]
    },
    {
    	"tables": [
    		"visit_report"
    	],
    	"authorizedRoles": [
            ["e7a567ba78c3daf97309b5ce48500c64", "d44ec2fe2b648365487118dbfb04a812"], // Reports, Staff
            ["e7a567ba78c3daf97309b5ce48500c64", "9370096149cfc279479c6fa6ce6e0da3"], // Reports, Supervisor
            ["e7a567ba78c3daf97309b5ce48500c64", "21692d7c7a28bde644ac6f015ca85e12"], // Reports, Manager
            ["45090411c65556c9fbf753427c05eb46", "21692d7c7a28bde644ac6f015ca85e12"], // Security, Manager
        ]
    }
];