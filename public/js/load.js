'use strict';

var URL = window.location.pathname.split('/').slice(0, -1).join('/');


function loadJS(file) {
    // DOM: Create the script element
    var jsElm = document.createElement("script");
    // set the type attribute
    jsElm.type = "application/javascript";
    // make the script element load file
    jsElm.src = file;
    jsElm.async = true;
    // finally insert the element to the body element in order to load the script
    document.body.appendChild(jsElm);
    console.log(jsElm);
}