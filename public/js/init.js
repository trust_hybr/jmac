// jquery and mobile initialization
$(document).bind("mobileinit", function() {
    $.mobile.allowCrossDomainPages = true;
    $.mobile.pushStateEnabled = false;
});

$(document).ready(function() {
    $.support.cors = true;
    $.ajaxSetup({ cache: false });
});