
'use strict';

// Put variables in global scope to make them available to the browser console.
const video = document.querySelector('#video_play');
const canvas = document.querySelector('#image_canvas');
window.canvas = canvas;

canvas.width = 480;
canvas.height = 360;

function handleSuccess(stream) {
  const videoTracks = stream.getVideoTracks();

  console.log('Got stream with constraints:', constraints);
  console.log(`Using video device: ${videoTracks[0].label}`);

  window.stream = stream; // make variable available to browser console
  video.srcObject = stream;
  video.play();
}

function handleError(error) {
  let v = constraints.video;
  if (error.name === 'ConstraintNotSatisfiedError') {
    errorMsg(`The resolution ${v.width.exact}x${v.height.exact} px is not supported by your device.`);
  } else if (error.name === 'PermissionDeniedError') {
    errorMsg('Permissions have not been granted to use your camera and ' +
      'microphone, you need to allow the page access to your devices in ' +
      'order for the demo to work.');
  } else if (error.name === 'NotFoundError') {
    errorMsg('Device not found ');    
  } else {
    errorMsg(`Error: ${error.name}`, error);  
  }
  consoleLog(["video =", v]);
  consoleLog(["error =", error]);
}

function errorMsg(msg, error) {
  const errorElement = document.querySelector('#camera_error_message');
  errorElement.innerHTML += '<p style="padding: 10px;">' + msg + '</p>';
  if (typeof error !== 'undefined') {
    console.error(error);
  }
}







let filterIndex = 0;
const filters = [
  'grayscale',
  'sepia',
  'blur',
  'brightness',
  'contrast',
  'hue-rotate',
  'hue-rotate2',
  'hue-rotate3',
  'saturate',
  'invert',
  ''
];

