'use strict';

var URL = window.location.pathname.split('/').slice(0, -1).join('/');
var bandsNetworkUrl = 'http://45.58.35.173:3000';

function consoleLog(a = []) {
    for (var i in a) {
        console.log(a[i]);
    }
}

function sleepFor(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
    /* await sleep(2000); // for 2 second delay */ 
}

function sleepFor2( sleepDuration ){
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration){ /* do nothing */ } 
}

// let csvContent = "data:text/csv;charset=utf-8," + truckData.map(e => e.join(",")).join("\n");

function getOnlineStatus() {
    var online = true;
    $.ajax({
       type: "GET",
       url: bandsNetworkUrl
    }).done(function(msg){
        $('#connection_type').html("Connection: " + window.navigator.connection.effectiveType + " |");
        // consoleLog('Connection: ' + window.navigator.connection.effectiveType);
    }).fail(function(XMLHttpRequest, textStatus, errorThrown) {
        alert('Please note: BandS server not reachable');
        $('#connection_type').html("Connection: BandS server not reachable |");
        // consoleLog('Connection: Not Connected', textStatus);
    });
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return '';
};

function getLocationOrigin() {
    return window.location.origin;
}

function countChar(val, limit, displayId) {
    var len = val.value.length;
    if (len >= limit) {
        val.value = val.value.substring(0, limit-1);
    } else {
        $('#' + displayId).html(limit - len);
    }
};

function showCharCount(val, displayId) {
    var len = val.value.length;
    $('#' + displayId).html(len);
};

function getUniqueId() {
    var d = new Date();
    return MD5(d.toISOString() + Math.random() + 'BandS unique id');
}

function getDbHandle() {
    var dbH = new Dexie("BandSDb");
    // blocks: "
    // _id: mongo id
    // notes (n): any extra info
    // ++record_id (i), 
    // other_involved_persons (o),
    // block_id (b),
    // synced (s), 
    // user_id (u), 
    // date_time (d), 
    // table_name (t), 
    // record (r), 
    // hash_of_previous_record (h), 
    // block_id_of_hashed_chained_record (c),
    // [table_name+synced] (complex indexing)"
    // birth scud on: birth speed start, give birth to fast work
    dbH.version(1).stores({
        blocks: "++i, b,r,t,h, s,c,u,d, o,n, [t+s]"
    });    
    return dbH;
}

function addZero(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}

function logoutUser() {
    consoleLog('')
    localStorage.removeItem('current_user_session_id');
    localStorage.removeItem('current_user_user_id');
    localStorage.removeItem('current_user_phone_number');
    localStorage.removeItem('current_user_name');
    localStorage.removeItem('current_user_roles');
}

function setUser(what, value) {
    localStorage.setItem(what, JSON.stringify(value));
}

function getUser(what = 'current_user_user_id') {
    if (what
        && localStorage.getItem(what)
        && localStorage.getItem(what) !== ''
        && localStorage.getItem(what) !== 'undefined'
    ) {
        console.log(what);
        return JSON.parse(localStorage.getItem(what));
    } else {
        logoutUser();
    }
    return '';
}

function getItemUlBox(tapLink, imageLink, title, summary, footer) {
    var itemStr = '';

    itemStr = itemStr + '<li class="jmacListItem">';
    itemStr = itemStr + '<a rel="external" href="' + tapLink + '">';
    itemStr = itemStr + '<img src="' + imageLink + '" />';
    itemStr = itemStr + '<h4>';
    itemStr = itemStr + title;
    itemStr = itemStr + '</h4>';
    itemStr = itemStr + '<p>';
    itemStr = itemStr + summary;
    itemStr = itemStr + '</p>';
    itemStr = itemStr + '</a>';
    itemStr = itemStr + '</li>';

    return itemStr;
}


function getItemTableBox(tapLink, imageLink, title, summary, footer) {
    var itemStr = '';

    itemStr = itemStr + '<span>';
    itemStr = itemStr + '<a style="text-decoration: none;" rel="external" href="' + tapLink + '">';
    itemStr = itemStr + '<table class="item_box">';
    itemStr = itemStr + '<tr>'; // row one
    
    itemStr = itemStr + '<td class="item_photo_box">'; 
    itemStr = itemStr + '<img src="' + imageLink + '" />';
    itemStr = itemStr + '</td>'; 
    
    itemStr = itemStr + '<td class="item_text_box">'; 
    itemStr = itemStr + '<b>' + title + '</b>';
    itemStr = itemStr + '&nbsp;&nbsp;<i>' + summary + '</i>';
    itemStr = itemStr + '</td>'; 

    itemStr = itemStr + '</tr>'; // row one
    itemStr = itemStr + '<tr>'; // row two

    itemStr = itemStr + '<td colspan="2">'; 
    itemStr = itemStr + footer;
    itemStr = itemStr + '</td>'; 

    itemStr = itemStr + '</tr>'; // row two
    itemStr = itemStr + '</table>';
    itemStr = itemStr + '</a>';
    itemStr = itemStr + '</span>';

    return itemStr;
}

function setRecordInfo(form, field_name, sequence, step, save_button_text, tried) {
    $("#form_detail").text(form['detail']);
    $("#windowTitle").text(form["title"]);
    $("#navbarTitle").html('<div onclick="document.location.reload()">' + form["title"] + '</div>');

    if (tried > 0) {
        $('#tried').text('Tried ' + tried + ' times. Maximum 4 tries allowed.');
    }

    consoleLog(['setRecordInfo step = ', step]);

    $("#label").html((parseInt(sequence) + 1) + ': <b>' + step["title"] + '</b>');
    var ml = '';
    if (step['minimumLength'] > 0) {
        ml = ' | At least type ' + step['minimumLength'] + ' charecter';
        if (step['minimumLength'] > 1) {
            ml = ml + 's'
        }
    }
    if (field_name === 'boolean') {
        ml = '';
    }
    if (field_name === 'integer') {
        $('#input_box_id').attr('min', step["min"]);
        $('#input_box_id').attr('max', step["max"]);
        $('#input_box_id').attr('step', step["step"]);
    }
    $("#detail").text(step["detail"] + ml);
    
    if (step["next_step_field"] === "final_step") {
        $("#next_button").text('Review');
    }
    if (field_name === "_save_step_") {
        $("#next_button").text(save_button_text); 
        // $("#next_button").text('Save');
    }
    
}

function idOfInsideUl(block, parentFieldName) {
    var id = block['r'][parentFieldName].toString().replace(' ', '_') + parentFieldName;
    consoleLog(['idOfInsideUl calculated id = ', id]);
    return  id;
}

function getTitlesForBlock(
    allForms, 
    formName, 
    block, 
    what = 'option', 
    link = '', 
    show = 'show_in_option',
    color = 'black'
) {
    consoleLog(['getTitlesForBlock allForms = ', allForms[formName]]);
    if (block && block['r']) {
        var field_name = allForms[formName]["first_step_field"];
        var row = '';
        var totalFields = allForms[formName]["steps"].length;

        var phoneNumber = '';
        for (var sequence = 0; sequence < totalFields; sequence++) {
            var isForeignKey = false;
            if (field_name === 'foreign_key') {
                field_name = allForms[formName]["steps"][sequence]["foreign_table"];
                isForeignKey = true;
            }

            var fn = field_name + '-' + allForms[formName]["steps"][sequence]["number"];

            if (field_name === 'phone_number') {
                phoneNumber = block['r'][fn];
            }            
            if (allForms[formName]["steps"][sequence][show] === true
                && fn && block && block['r'] && block['r'][fn]
            ) {
                var value = block['r'][fn];
                if (isForeignKey || block['r'][fn + '-detail']) {
                    value = block['r'][fn + '-detail'];
                }
                if (what === 'field_table') {
                    row = row
                        + '<tr class="jmacListItem" style="color: ' + color + ';"><th><b>' 
                        + allForms[formName]["steps"][sequence]["title"] + '</b></th><td>'
                        +  value
                        + '</td></tr>'
                } else if (what === 'plain_option' || what === 'li_with_a') {
                    row = row 
                        + value
                        + ' | ';                    
                } else {
                    row = row 
                        + '<b>' + allForms[formName]["steps"][sequence]["title"] + '</b> : ' 
                        + value
                        + ' | ';
                }
            }
            field_name = allForms[formName]["steps"][sequence]["next_step_field"];
        }
      
        var ulTag = '';
        row = row.replace(/ \| $/, '');
        if (what === 'option' || what === 'plain_option') {
            if (allForms[formName]['parent_field'] !== '') {
                ulTag = '__TREE_INDICATOR__';
            }
            return '<option style="color: ' 
                + color 
                + ';" value="' 
                + block['b'] 
                + '">' 
                + ulTag
                + row 
                + '</option>';
        } else if (what === 'li_with_a') {
            if (allForms[formName]['parent_field'] !== '') {
                ulTag = '<ul id="'  + block['b']  + '"></ul>';
            }
            var callTag = '';
            if (formName === 'contact') {
                callTag = '&nbsp;&nbsp;<a href="tel:+' + phoneNumber 
                    + '"><img width="20px" src="images/icons/Phone_96px.png" /></a>';
            }
            var bid = '';
            var showBlockId = getUrlParameter('bid') || false;
            if (showBlockId) {
                bid = ' ' + block['b'];
            }
            return '<li><a style="color: ' 
                + color 
                + ';" rel="external" href="' + link + '">' 
                + row 
                + '</a>'
                + bid
                + callTag
                + ulTag
                + '</li>';
            return  ulTag;
        } else if (what === 'field_table') {
            return row;
        } else {
            return row;
        }
    }
    return '';
}

function setFormDefault(default_value, input_id) {
    consoleLog(['setFormDefault Default value = ', default_value]);
    if (default_value === "_now_date_") {
        var now = new Date();
        $('#' + input_id).val(addZero(now.getYear()+1900) + '-' + addZero(now.getMonth()+1) + '-' + addZero(now.getDate())).change();
    } else if (default_value === '_current_user_') {
        if (
            ('current_user_phone_number') !== '') {
            $('#' + input_id).val(getUser('current_user_phone_number')).change();
        } else {
            $('#' + input_id).val("").change();
        }
    } else if (default_value === "_now_time_") {
        var now = new Date();
        $('#' + input_id).val(addZero(now.getHours()) + ':' + addZero(now.getMinutes())).change();        
    } else {
        $('#' + input_id).val(default_value).change();
    }
     $("label:first").click();
}

function setDbDefault(rid, step_number, field_name, input_id) {
    var dbH = getDbHandle();
    consoleLog(['setDbDefault rid = ', rid]);
    if (rid === 'undefined' || typeof(rid) === undefined) { return; }
    dbH.blocks.get(parseInt(rid), function(block) {
        if (block 
            && block['r'] 
            && block['r'][field_name + '-' + step_number]
        ) {
            var v = block['r'][field_name + '-' + step_number];
            if (v && v.length >= 1) {
                consoleLog(['setDbDefault value = ', v]);
                if(['org_location', 'autocomplete'].indexOf(field_name) !== -1) {
                    var vd = block['r'][field_name + '-' + step_number + '-detail'];
                    $('#input_box_id_query').val(vd).change();

                    $('#input_box_id').children().find('input').each(function(i, e){
                        var iva = v.split('|');
                        if ($(e).val() === iva[0]) {
                            $(e).attr('checked','checked').change();        
                        }
                    });
                }else {
                    $('#' + input_id).val(v).change();
                    $('#' + input_id + ' option[value="' + block['b'] + '"]').attr('selected','selected').change();
                }
                $('#next_button').removeAttr('disabled');
            } else {
                $('#next_button').attr('disabled', 'true');
            }
        }
    });
}

function checkRequired(requird, input_id, minimumLength) {
    if (requird === true) { 
        $('#next_button').attr('disabled', 'true');
        var v = $('#' + input_id).val();
        if (v && v.length >= minimumLength) {
            $('#next_button').removeAttr('disabled');
        } else {
            $('#next_button').attr('disabled', 'true');
        }        
        $(document).on(' click select change keyup ','#' + input_id, function() {
            var v = $('#' + input_id).val();
            if (!v) {
                // value of radio button
                v = $('#input_box_id').children().find('input:checked').val();
            }
            consoleLog([
                'value = ',
                v
            ]);
            if (v && v.length >= minimumLength) {
                $('#next_button').removeAttr('disabled');
            } else {
                $('#next_button').attr('disabled', 'true');
            }
        });
    }
}

function saveValue(form, rid, sequence, field_name, input_id, formName, tried = 0) {
    $(document).on('click',"#next_button", function() {
        // consoleLog('field_name = ', field_name);
        event.preventDefault();
        var dbH = getDbHandle();
        dbH.transaction("rw", dbH.blocks, function () {
            var fn = field_name + '-' + form["steps"][sequence]["number"];
            dbH.blocks.get(parseInt(rid), function(blockToBeUpdated) {
                if (!(blockToBeUpdated && blockToBeUpdated['r'] && blockToBeUpdated['o'])) {
                    alert("Error in updating data. Fixing it.");
                    dbH.delete().then(() => {
                        consoleLog(["Database successfully deleted"]);
                        dbH = getDbHandle();
                    }).catch((err) => {
                        console.error("Could not delete database");
                    }).finally(() => {
                        window.history.go(-2);
                    });
                    return;
                }
                var d = $('#input_box_id' + ' option:selected').text();

                if (field_name === 'radio' 
                    || field_name === 'org_location'
                    || field_name === 'autocomplete'
                    
                ) {
                    $('#input_box_id').children().find('input').each(function(i, e){
                        if ($(e).is(":checked")) {
                            var inputValue = $(e).val();
                            d = $("label[for='"+$(e).attr("id")+"']").text();
                            if (field_name === 'org_location') {
                                var iva = inputValue.split('|');
                                inputValue = iva[1];
                                blockToBeUpdated['r']['organization-0'] = iva[0];
                            } else if (field_name === 'autocomplete') {
                                var iva = inputValue.split('|');
                                inputValue = iva[1];
                                blockToBeUpdated['r']['user-0'] = iva[0];
                            }                            
                            blockToBeUpdated['r'][fn] = inputValue;
                        }
                    });
                } else {
                    blockToBeUpdated['r'][fn] = $('#input_box_id').val();
                }

                if (d !== '') {
                    blockToBeUpdated['r'][fn + '-detail'] = d;
                }
                if (fn === 'phone_number') {
                    if (blockToBeUpdated['o']) {
                        blockToBeUpdated['o'].push(v);
                    } else {
                        blockToBeUpdated['o'] = [v];
                    }
                }
                dbH.blocks.where('i').equals(parseInt(rid)).modify({
                    r: blockToBeUpdated['r'],
                    o: blockToBeUpdated['o']
                }). then(function() {
                    var nextStep = parseInt(sequence) + 1;
                    var next_field_name = form["steps"][sequence]["next_step_field"];
                    if (nextStep === form["steps"].length) {
                        next_field_name = '/../final/' + next_field_name;
                    } else {
                        next_field_name = '/../fields/' + next_field_name;
                    }
                    window.location.assign(URL + next_field_name + '.html?f=' + formName + '&s=' + nextStep + '&r=' + rid + '&t=' + tried);
                }).catch (Dexie.ModifyError, function (e) {
                    // ModifyError did occur
                    console.error(e.failures.length + " field failed to get modified ", e);
                }).catch (function (e) {
                    console.error("Generic error modify saveValue : " + e);
                }); /* dbH.blocks.where('i').equals(parseInt(rid)).modify */
            });
 
          }).catch (function (e) {
            console.error("Generic error saveValue transaction : " + e);
        }); /* dbH.transaction("rw", dbH.blocks */
        
    }); // $(document).on('click',"#next_button"
}

function processForm(field_name, rid, formName, form, sequence, tried = 0) {

    setRecordInfo(
        form,
        field_name,
        sequence,
        form["steps"][sequence],
        form["save_button_text"],
        tried
    );

    setFormDefault(
        form["steps"][sequence]["default_value"], 
        "input_box_id"
    );

    setDbDefault(
        rid, 
        form["steps"][sequence]["number"], 
        field_name, 
        "input_box_id"
    );

    checkRequired(
        form["steps"][sequence]["required"],
        "input_box_id",
        form["steps"][sequence]["minimumLength"]
    );

    saveValue(
        form,
        rid,
        sequence,
        field_name, 
        "input_box_id", 
        formName,
        tried
    );

}

function getHomePage() {
    return URL.replace('fields','').replace('reports', '').replace('final', '') + "/index.html";
}

$(document).ready(function(){


    $(document)

    // .ajaxStart(function() {
    //     $.mobile.loading( "show", {
    //         text: "loading...",
    //         textVisible: true,
    //         theme: "b",
    //         html: ""
    //     });
    // })

    // .ajaxStop(function() {
    //     $.mobile.loading( "hide" );
    // })

    .on("click", "#home_page_image", function(data) {
        event.preventDefault();
        // consoleLog('home page dir = ', URL.replace('fields',''));
        window.location.assign(getHomePage());
    }).on ("click", "#open_camera", function(e) {
        e.target.disabled = true;
        const constraints = window.constraints = {
            audio: false,
            video: true
        };
        navigator.mediaDevices.getUserMedia(constraints).then(handleSuccess).catch(handleError);
    }).on("click", "#take_snapshot", function(data) {
        const video = document.querySelector('#video_play');
        const canvas = document.querySelector('#image_canvas');
        window.canvas = canvas;
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
    });

    if (!(getUser('current_user_session_id')
        && getUser('current_user_session_id') !== '')
    ) {
        // send user to login page
        consoleLog([
            'current_user_session_id not exists'
        ]);
        if (window.location.href.toLowerCase().indexOf('index.html') === -1) {
            if (window.location.href.toLowerCase().indexOf('user_login') === -1) {
                window.location.assign(getHomePage() + "?m=no_login");
            }
        }
    } else {
        consoleLog([
            'syncronizing started'
        ]);
        syncDb();        
    }
}); /* $(document).ready(function() */

async function addBlockInLocalDb(block) {
    var dbH = getDbHandle();
    await dbH.blocks.add(block)
        .catch (function (e) {
            console.error("Generic error add block : " + e);
        });
}

async function updateBlockInLocalDb(remoteBlock) {
    var dbH = getDbHandle();
    var rmd5 = MD5(JSON.stringify(remoteBlock['r']));
    var synced = false;
    // consoleLog('---- remoteBlock = ', remoteBlock['b'], remoteBlock);
    await dbH.transaction("rw", dbH.blocks, function () {

        dbH.blocks.each(function(existingBlock) {
            
            if (existingBlock['b'] == remoteBlock['b']
                && existingBlock['h'] == remoteBlock['h'] 
                && parseInt(existingBlock['s']) === 1
            ) {
                // consoleLog('existingBlock block id match = ', remoteBlock['b'], existingBlock);
                var emd5 = MD5(JSON.stringify(existingBlock['r']));
                if (emd5 !== rmd5) {
                    // delete
                    // consoleLog('existingBlock record does not match = ', remoteBlock['b'], existingBlock);
                    dbH.blocks.where('b').equals(existingBlock['b'])
                    .modify(function(b) {
                        b['r'] = remoteBlock['r'];
                        // consoleLog('update existingBlock to remoteBlock = ', remoteBlock['b'], remoteBlock);
                    }).catch (function (e) {
                        console.error("Generic error update block : " + e);
                    });
                }
                synced = true;
            }
        }).then(function() {
            if (!synced) {
                // consoleLog('new add of remoteBlock = ', remoteBlock['b'], remoteBlock);
                addBlockInLocalDb(remoteBlock);                
            }
        }).catch (function (e) {
            console.error("Generic error update block : " + e);
        });
    });
}

var localRecords = [];

async function getAllLocalRecords() {
    var dbH = getDbHandle();
    await dbH.transaction("rw", dbH.blocks, function () {
        dbH.blocks.each(function(existingBlock) {
            localRecords.push(existingBlock['b']);
        }); // dbH.blocks.get({"b": response[i]['b']})
    }).catch (function (e) {
        console.error("Generic error delete transaction : " + e);
    }); /* dbH.transaction("rw", dbH.blocks */    
}

function syncDb() {
    if (getUser('current_user_session_id') === '') {
        return;
    }
    var dbH = getDbHandle();
    // consoleLog('syncing started');
    dbH.transaction("rw", dbH.blocks, function () {
        dbH.blocks.where("s").equals(0).each(function(blockToSync) {
            // consoleLog('sync start for block ', blockToSync);
            if (blockToSync['t'] !== 'user_login') {
                $.ajax({
                    type : 'POST',
                    dataType : 'json',
                    async: true,
                    url: bandsNetworkUrl + '/mb',
                    headers: {
                        "bands_sid" : getUser('current_user_session_id')
                    },
                    data: {
                        "blockToSync" : JSON.stringify(blockToSync)
                    }
                }).done(function(addedStatus) { /* ajax */
                    
                    consoleLog(['syncDb addedStatus =', addedStatus]);
                    if (addedStatus['message'] === 'added') {
                        dbH.blocks.where("i").equals(blockToSync['i'])
                            .modify(function(blockToModify) {
                                blockToModify['s'] = 1;
                        });
                    } else if (addedStatus['message'] === 'invalid sid') {
                        logoutUser();
                        alert('login expired, please login again');
                        window.location.assign(getHomePage());
                    }
                    
                }).fail(function(XMLHttpRequest, textStatus, errorThrown) { /* ajax */
                    alert(textStatus + ' ' + errorThrown);
                    // consoleLog(errorThrown);
                }); /* ajax */ 
            } // if (blockToSync['t'] !== 'user_login')

        }) // dbH.blocks.where("s").equals(0).each(function(block)
        .then(function(){
            $.ajax({
                type : 'GET',
                dataType : 'json',
                async: true,
                url: bandsNetworkUrl + '/mb',
                headers: {
                    "bands_sid" : getUser('current_user_session_id')
                }
            }).done(function(response) { /* ajax */
                // getAllLocalRecords();
                if (response['message'] === 'invalid sid') {
                    logoutUser();
                    alert('login expired, please login again');
                    window.location.assign(getHomePage());
                }                
                for (var i = 0; i < response.length; i++) {
                    updateBlockInLocalDb(response[i]);
                }
            }).fail(function(XMLHttpRequest, textStatus, errorThrown) { /* ajax */
                alert(textStatus + ' ' + errorThrown);
                // consoleLog(errorThrown);
            }); /* ajax */             
        })
    }).catch (function (e) {
        console.error("Generic error syncdb set block transaction : " + e);
    }); /* dbH.transaction("rw", dbH.blocks */

    // get all thoes records with are not created by me but are for me

    // avoide user_login table, as those are already synced as part of the login process
    // find records with synced  value 0 and write them remotely
    // get all thoes records with are not created by me but are for me



    // if this is force update then copy records which are creaed by me
}